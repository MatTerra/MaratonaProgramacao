# Regras de Desenvolvimento 

## Variáveis      

  * Se globais, devem ser declaradas em Camel Case com a primeira letra maiúscula conforme o exemplo:
```c
    float VolumeDoCilindro = PI*Raio*Raio;
```

  * Se variáveis, devem ser declaradas em Camel Case com a primeira letra minúscula conforme o exemplo:
```c
    int raioDoCirculo = 3;
```

  * Se estáticas, devem ser declaradas em Camel Case precedidas por '_', observando-se as regras anteriores, conforme o exemplo:
```c
    static int _compVertical = 3;
```     

  * Se constantes, devem ser declaradas como macros.
  * Os nomes devem ter comprimento menor ou igual a 15 caracteres e DEVEM SER SUGESTIVOS!

## Macros

  * Devem ser declaradas com todas as letras maiúsculas conforme o exemplo:

```c
    #define PI 3.14
```  

## Funções

   * Se tem acesso geral, devem ser declaradas em Camel Case com a primeira letra minúscula conforme exemplo:
```c
    int soma(x, y){
        ...
    }
```

   * Se tem acesso restrito, devem ser declaradas em Camel Case precedidos por '_' conforme exemplo:
```c
    int _mult(int x, y){
        /* usada apenas dentro da funcao elevar */
        ...
    }
```

## Formatação

   * If's, Whiles's e For's com apenas um comando devem ser declarados sem chaves com tabulação simples conforme o exemplo:
   * Após a declaração de funções, deve haver uma linha em branco antes de outras funções ou variáveis conforme o exemplo:     
```c
        ...
    }
    
    int soma(){
        ...
    } 
```

   * A tabulação deve ser feita com 4 espaços.
   * É preferível a declaração de todas as variáveis de um mesmo tipo juntas, uma por linha precedidas por meia tabulação, no início do escopo, conforme exemplo:
```c
    int main(){
        int x = 0,
          y = 0;
        ...
    } 
```

   * A declaração de vetores bidimensionais deve ser feita com linhas separadas para cada vetor da segunda dimensão, com tabulação para alinhar as colunas, conforme exemplo:
```c
    int vetor[2][2] = {{0, 0},
                       {0, 0}};
```

   * Operadores devem ser precedidos e sucedidos de um, e apenas um, espaço em branco.
   * Parênteses, Chaves e Colchetes não devem ser precedidos, nem sucedidos de espaços em branco.
   * Comentários de documentação dos argumentos e retorno da função devem preceder a declaração da função, com dois asteriscos na abertura. Não devem ter texto na primeira linha e devem seguir o modelo:
```c
    /**
     *     @name: maior
     *   @author: Mateus Berardo
     *  @version: 1.0
     *
     *
     * @return int:
     *      O maior dos operandos ou 0 se forem iguais
     *
     * @param x1:
     *     primeiro operando
     * @param x2:
     *     segundo operando
     *
     */ 
```    